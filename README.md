# hieloapp.com
## In order for communities to thrive in they must first live without fear.

This is my first attempt at open-source project to help crowdsource the location of proposed, active, & recent raids conducted by the federal agency commonly known as ICE.

## Why:
_It is the right thing to do._ 
Sources to come but time over time communiites that rightfully distrust local law enforment and federal agencies suffer more unreported crime than those that do not. As we've seen from history there are long lasting - generational effects left on those members of the community.

## How:
I invision a public service like [waze](https://www.waze.com/) and [fogocruazdo](https://fogocruzado.org.br/sobre/) _link only provided for reference_.


## Needed:
- much more capable people than what I alone can do.
- motivated people driving by social justice.
- people who want to be able to tell their children someday that they at least tried to do something.

## Contact:
- Twitter: @clgfather
- Gitlab: @ernestozgutierrez
- Email: ernesto.z.gutierrez@gmail.com - _i'm sure i'll regret this_